module Referee.Mastermind.Game
    ( Combination
    , Game(..)
    , Result(..)
    , Reason(..)
    , newGame
    , turn
    ) where


import           Data.Function ((&))
import           Data.List     as List
import qualified System.Random as Rnd

type Combination = String


data Reason
    = TooSmall
    | TooBig
    | TooManyAttempt
    | BadColor
    deriving (Show, Eq)


data Result
    = Result Int Int
    | Found
    | Failed Reason
    deriving (Show, Eq)


data Game
    = Game
    { limit  :: Int
    , size   :: Int
    , colors :: String
    , secret :: Combination}


newGame :: Int -> Int -> String -> Rnd.StdGen -> Game
newGame limit size colors gen
    = Game
    { limit = limit
    , size = size
    , colors = colors
    , secret = Rnd.randomRs (0, length colors - 1) gen
             & List.map (colors !!)
             & List.take size
    }


hasValidColors :: Game -> Combination -> Bool
hasValidColors game guess = not $ null ((guess & List.nub) List.\\ colors game)


missplacedCount :: String -> String -> Int
missplacedCount _ [] = 0
missplacedCount secret (head:tail)
    | head `List.elem` secret = 1 + missplacedCount (List.delete head secret) tail
    | otherwise               = missplacedCount secret tail


turn :: Game -> Int -> Combination -> Result
turn game turn guess
    | turn >= limit game                  = Failed TooManyAttempt
    | length guess < length (secret game) = Failed TooSmall
    | length guess > length (secret game) = Failed TooBig
    | hasValidColors game guess           = Failed BadColor
    | otherwise                           =
    let
        correct = (length . filter (uncurry (==)) . List.zip (secret game)) guess
        missplaced =
            (uncurry missplacedCount
            . List.unzip
            . filter (uncurry (/=))
            . List.zip (secret game)) guess
    in
        if correct == length (secret game)
            then Found
            else Result correct missplaced

