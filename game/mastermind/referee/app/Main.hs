{-# LANGUAGE QuasiQuotes #-}
module Main where


import           Data.String.Interpolate (i)
import qualified Referee.Command         as Command
import qualified Referee.Mastermind.Game as Game
import           System.IO
import qualified System.Random           as Rnd


size :: Int
size = 8


colors :: String
colors = "ABCDEFGH"


maxTurn :: Int
maxTurn = 50


render :: Game.Result -> String
render Game.Found                        = "@0:Found."
render (Game.Failed Game.TooSmall)       = "@0:combination too small"
render (Game.Failed Game.TooBig)         = "@0:combination too big"
render (Game.Failed Game.BadColor)       = "@0:bad color"
render (Game.Failed Game.TooManyAttempt) = "@0:too many attempt"
render (Game.Result correct missplaced)  = [i|@0:#{correct} #{missplaced}|]


loop :: Game.Game -> Int -> IO ()
loop game t
    = do
        putStrLn [i|/turn: #{t} 0 1 50|]
        command <- fmap Command.parse getLine
        case command of
            Command.Message _ guess -> do
                let result = Game.turn game t guess
                putStrLn $ render result
                case result of
                    Game.Found    -> putStrLn "/winners: 0"
                    Game.Failed _ -> putStrLn "/winners:"
                    _             -> loop game (t+1)
            Command.Error msg -> do
                hPutStrLn stderr [i|error: #{msg}|]
                putStrLn "/winners:"
            Command.Timeout id -> do
                putStrLn [i|/drop: #{id}|]
                putStrLn "/winners:"


main :: IO ()
main = do
    hSetBuffering stdout NoBuffering
    putStrLn [i|@@:#{size} #{colors} #{maxTurn}|]
    game <- fmap (Game.newGame maxTurn size colors) Rnd.getStdGen
    hPutStrLn stderr [i|secret selected #{Game.secret game}|]
    loop game 1
