module Referee.Mastermind.GameSpec (spec) where


import qualified Data.List               as List
import           Referee.Mastermind.Game
import           Test.Hspec              (Spec, it, shouldBe)
import           Test.QuickCheck         (NonNegative (..), Positive (..),
                                          property)


evaluateCombination :: Combination -> Combination -> Result
evaluateCombination secret = turn Game
    { limit = 10
    , size = 4
    , colors = "ABCDEF"
    , secret = secret
    } 1


buildCombination :: Int -> Char -> Int -> Char -> Int -> Combination
buildCombination countA a countF filler index =
    let secret = replicate countA a ++ replicate countF filler
    in
        List.permutations secret !! (index `mod` (countA + countF))


spec :: Spec
spec = do

    it "should evaluate a correct solution" $
        evaluateCombination "ACEF" "ACEF" `shouldBe` Found


    it "should evaluate a solution with exact match" $ do
        evaluateCombination "ACCC" "ABBB" `shouldBe` Result 1 0
        evaluateCombination "AACC" "AABB" `shouldBe` Result 2 0
        evaluateCombination "CAAA" "BAAA" `shouldBe` Result 3 0
        evaluateCombination "AAAA" "ABBB" `shouldBe` Result 1 0


    it "should satisfy property" $ property $
        \(Positive ix, Positive iy, NonNegative i, NonNegative j) ->
            let
                x = ix `mod` 500
                y = iy `mod` 500
                secret = buildCombination x 'A' y 'B' i
                guess = buildCombination x 'A' y 'C' j
                Result a b = evaluateCombination secret guess
            in a + b == x


    it "should evaluate a solution with miss placed match" $ do
        evaluateCombination "ACCC" "BAAA" `shouldBe` Result 0 1
        evaluateCombination "ACCA" "AAAB" `shouldBe` Result 1 1
        evaluateCombination "ACCA" "BAAB" `shouldBe` Result 0 2


    it "should evaluate a too small solution" $
        evaluateCombination "ACCC" "A" `shouldBe` Failed TooSmall


    it "should evaluate a too big solution" $
        evaluateCombination "ACCC" "AAAAA" `shouldBe` Failed TooBig


    it "should evaluate a bad color in solution" $
        evaluateCombination "ACCC" "ZCCC" `shouldBe` Failed BadColor


    it "should evaluate too many attempt" $
        turn
            Game { limit = 10, size = 4, colors = "ABCDEF", secret = "ACCC" }
            10
            "ACCC"
            `shouldBe` Failed TooManyAttempt
