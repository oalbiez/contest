module Main where


import           Data.String.Utils (strip)
import qualified System.IO         as IO


main :: IO ()
main = do
    IO.stdout `IO.hSetBuffering` IO.LineBuffering
    fmap (strip . drop 5) getLine >>= putStrLn . ("Hello " ++)
