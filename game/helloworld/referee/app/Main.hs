{-# LANGUAGE QuasiQuotes #-}
module Main where


import           Data.String.Interpolate       (i)
import qualified Referee.Command               as Command
import           Referee.Helloworld.Firstnames
import qualified System.IO                     as IO


main :: IO ()
main = do
    IO.stdout `IO.hSetBuffering` IO.LineBuffering
    firstname <- chooseFirstname
    putStrLn $ "@0:I'am " ++ firstname
    putStrLn "/turn: 0 0 1 50"
    command <- fmap Command.parse getLine
    case command of
        Command.Message _ reply ->
            if reply == "Hello " ++ firstname
            then
                putStrLn "/winners: 0"
            else
                putStrLn "/winners:"
        Command.Error msg -> do
            IO.hPutStrLn IO.stderr [i|error: #{msg}|]
            putStrLn "/winners:"
        Command.Timeout id -> do
            putStrLn [i|/drop: #{id}|]
            putStrLn "/winners:"
