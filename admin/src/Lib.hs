{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeOperators         #-}

module Lib
    ( startApp
    , app
    ) where

import           Business.Identifier
import           Business.Trigger.Model
import           Business.Trigger.Usecase
import           Control.Concurrent.STM
import           Control.Monad.IO.Class
import           Data.Aeson                        hiding (Result)
import           Data.Aeson.TH
import qualified Data.IORef                        as Ref
import qualified Data.Map                          as M
import           Infra.Spi.IdentifierGenerator     as IdentifierGenerator
import           Infra.Spi.IdentifierGenerator     (uuidGenerator)
import qualified Infra.Spi.MemoryResultRepository  as MemoryResultRepository
import qualified Infra.Spi.MemoryTriggerRepository as MemoryTriggerRepository
import           Network.Wai
import           Network.Wai.Handler.Warp
import           Servant
import           Zcap


newtype User = User
  { userName :: String
  } deriving (Eq, Show)
$(deriveJSON defaultOptions ''User)


newtype State = State {
  runningTriggers :: TVar (M.Map (Identifier Trigger) (TVar TriggerStatus))
}




{-}
atomically :: STM a -> IO a
newTVarIO :: a -> IO (TVar a)
newTVar :: a -> STM (TVar a)
readTVar :: TVar a -> STM a
writeTVar :: TVar a -> a -> STM ()

atomically $ do
  triggerState <- readTVar ....
  pure $ ...
  -}

type Store a = Ref.IORef (M.Map String a)

findObject :: MonadIO m => Store a -> String -> m (Maybe a)
findObject store id = do
  objects <- liftIO $ Ref.readIORef store
  return $ M.lookup id objects



type API = "user" :> Capture "id" String :> Get '[JSON] User
           :<|> "trigger" :> Capture "id" String :> Post '[JSON] String

startApp :: IO ()
startApp = do
  users <- Ref.newIORef M.empty
  triggers <- MemoryTriggerRepository.empty
  results <- MemoryResultRepository.empty
  _ <- createTrigger triggers uuidGenerator "oalbiez" "polop" "referee"

  run 8090 $ app users triggers results

app :: (TriggerRepository Handler tr, ResultRepository Handler rr) => Store User -> tr -> rr -> Application
app users triggers results = serve api $ server users triggers results

api :: Proxy API
api = Proxy

server :: (TriggerRepository Handler tr, ResultRepository Handler rr) => Store User -> tr -> rr -> Server API
server users triggers results = getUser users :<|> postTrig triggers results uuidGenerator

getUser :: Store User -> String -> Handler User
getUser store id = do
  mUser <- findObject store id
  case mUser of
    Just user ->
        return user

    Nothing ->
        throwError $ err404 { errBody = "no user with that id" }

postTrig :: (TriggerRepository Handler tr, ResultRepository Handler rr) => tr -> rr -> IdGenerator Handler Result -> String -> Handler String
postTrig tstore rstore idgen id = do
  result <- trig tstore rstore idgen (mkIdentifier id)
  case result of
    Just message ->
      return $ showTriggerStatus message

    Nothing ->
      return "paf"



--  return $ User "yntmYh-2KUTKttVeI_1ZYQ==" "Isaac Newton"
