{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module Infra.Spi.MemoryResultRepository (
    empty
) where

import           Business.Identifier
import           Business.Trigger.Model
import           Business.Trigger.Usecase
import           Control.Monad.IO.Class
import           Data.Cache               as Cache


newtype MemoryResultRepository = MemoryResultRepository
    { content :: Cache.Cache String Result
    }


empty :: MonadIO m => m MemoryResultRepository
empty =
    liftIO $ MemoryResultRepository <$> Cache.newCache Nothing


instance MonadIO m => ResultRepository m MemoryResultRepository
    where
        createResult repo rid trigger = do
            let result = Result rid (username trigger) (container trigger) (referee trigger) False ""
            liftIO $ Cache.insert (content repo) (unIdentifier rid) result
            return True
        findResult repo rid = liftIO $ Cache.lookup (content repo) (unIdentifier rid)
        updateResult repo rid status logs = liftIO $ do
            result <- Cache.lookup (content repo) (unIdentifier rid)
            case result of
                Just r -> do
                    Cache.insert (content repo) (unIdentifier rid) $ r { rstatus = status, rlog = logs}
                    return True
                Nothing -> return False
