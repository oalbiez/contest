{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module Infra.Spi.IdentifierGenerator (
    uuidGenerator
) where

import           Business.Identifier
import           Control.Monad.IO.Class
import           Data.UUID
import           Data.UUID.V4


uuidGenerator :: MonadIO m => m (Identifier a)
uuidGenerator =
    liftIO $ mkIdentifier . toString <$> nextRandom
