{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module Infra.Spi.MemoryTriggerRepository (
    empty
) where

import           Business.Identifier
import           Business.Trigger.Model
import           Business.Trigger.Usecase
import           Control.Monad.IO.Class
import           Data.Cache               as Cache


newtype MemoryTriggerRepository = MemoryTriggerRepository
    { content :: Cache.Cache String Trigger
    }


empty :: MonadIO m => m MemoryTriggerRepository
empty =
    liftIO $ MemoryTriggerRepository <$> Cache.newCache Nothing


instance MonadIO m => TriggerRepository m MemoryTriggerRepository
    where
        delete repo pid = do
            liftIO $ Cache.delete (content repo) (unIdentifier pid)
            return True
        save repo trigger = do
            liftIO $ Cache.insert (content repo) (unIdentifier (identifier trigger)) trigger
            return trigger
        find repo pid = liftIO $ Cache.lookup (content repo) (unIdentifier pid)
