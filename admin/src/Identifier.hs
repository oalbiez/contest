module Identifier
  ( Identifier,
    mkIdentifier,
    unIdentifier,
  ) where


newtype Identifier a = Identifier String deriving (Show, Read, Eq)


mkIdentifier :: String -> Identifier a
mkIdentifier = Identifier


unIdentifier :: Identifier a -> String
unIdentifier (Identifier value) = value
