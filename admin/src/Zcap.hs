module Zcap where

import           Data.ByteString.Base64.URL (encodeBase64)
import           Data.ByteString.Random.MWC (random)
import qualified Data.Text                  as T

makeZcap :: IO String
makeZcap = do
  bytes <- random 16
  return $ T.unpack $ encodeBase64 bytes
