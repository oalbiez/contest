{-# LANGUAGE MultiParamTypeClasses #-}

module Business.Trigger.Usecase where

import           Business.Identifier
import           Business.Trigger.Model
import           Control.Concurrent.Async
import           Control.Monad.IO.Class


class MonadIO m => ResultRepository m repo where
    createResult:: repo -> Identifier Result -> Trigger -> m Bool
    findResult :: repo -> Identifier Result -> m (Maybe Result)
    updateResult :: repo -> Identifier Result -> Bool -> String -> m Bool


class MonadIO m => TriggerRepository m repo where
    delete :: repo -> Identifier Trigger -> m Bool
    save :: repo -> Trigger -> m Trigger
    find :: repo -> Identifier Trigger -> m (Maybe Trigger)


type IdGenerator m a = m (Identifier a)


trig :: (TriggerRepository m tr, ResultRepository m rr) => tr -> rr -> IdGenerator m  Result -> Identifier Trigger -> m (Maybe TriggerStatus)
trig trepo rrepo idgen uuid = do
    trigger <- find trepo uuid
    case trigger of
        Just t  -> do
            result_id <- idgen
            succed <- createResult rrepo result_id t
            if succed
                then
                    -- run async sur le trigger et result_id
                    -- renvoyer result_id
                    liftIO $ Just . RunningTrigger <$> async (runTrigger t)
                else
                    pure Nothing
        Nothing -> return Nothing

runTrigger :: Trigger -> IO (Bool, String)
runTrigger trigger =
    pure (False, "not executed, actually")


createTrigger :: TriggerRepository m r => r -> IdGenerator m Trigger -> String -> String -> String -> m Trigger
createTrigger repo idgen username container referee = do
    id <- idgen
    save repo (Trigger id username container referee)


deleteTrigger :: TriggerRepository m r => r -> Identifier Trigger -> m Bool
deleteTrigger = delete

showTriggerStatus :: TriggerStatus -> String
showTriggerStatus (RunningTrigger _) = "Running"
showTriggerStatus (FinishedTrigger success message) = (if success then "Success" else "Failed") <> " (" <> message <> ")"
