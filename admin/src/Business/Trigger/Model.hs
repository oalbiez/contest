{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}
module Business.Trigger.Model
  (
      Trigger(..),
      Result(..),
      TriggerStatus(..)
  ) where


import           Business.Identifier
import           Control.Concurrent.Async
import           GHC.Generics


data Trigger = Trigger
  { identifier :: Identifier Trigger
  , username   :: String
  , container  :: String
  , referee    :: String
  } deriving (Show, Read, Generic)


data TriggerStatus = RunningTrigger (Async (Bool, String))
                   | FinishedTrigger Bool String


data Result = Result
  { ridentifier :: Identifier Result
  , rusername   :: String
  , rcontainer  :: String
  , rreferee    :: String
  , rstatus     :: Bool
  , rlog        :: String
  } deriving (Show, Read, Generic)
