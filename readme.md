[![pipeline status](https://gitlab.com/oalbiez/contest/badges/master/pipeline.svg)](https://gitlab.com/oalbiez/contest/commits/master)

# Contest


## TODO


trig -> job
job -> result

Est que on garde TriggerRepo, ResultRepo JobRepo ? ou est-ce que l'on fait une interface adhoc ?


- Faire le site d'administration
- Faire le server
- Faire un renderer HTML d'un match
- Faire différents player de mastermind


## Ideas

- Faire des jeux multijoueurs


## Questions

- Quelle licence pour le code des participants ?


## Notes

### Ranking

- https://www.microsoft.com/en-us/research/project/trueskill-ranking-system/
- https://fr.wikipedia.org/wiki/Classement_Glicko


### Architecture

#### admin

Ce compostant sert à gérer :
- liste des joueurs
- la liste des triggers
- le résultat des matchs


#### match

Ce composant sert à gérer un match avec un arbitre et des joueurs


### Protocole de communication

    sur stdout:
    @0: vers/du le joueur 0
    @1: vers/du le joueur 1
    @@: vers/du tous les joueur
    /command: commande vers arbitre ou vers controleur

    sur stderr: pour du log
