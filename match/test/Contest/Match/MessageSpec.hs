module Contest.Match.MessageSpec (spec) where


import           Contest.Match.Message
import           Test.Hspec


spec :: Spec
spec = do

    it "route should parse a command message" $ do
        routeToMessage ["/cmd"] `shouldMatchList` [Command "cmd"]
        routeToMessage ["/polop a b c"] `shouldMatchList` [Command "polop a b c"]

    it "route should parse all message" $ do
        routeToMessage ["@@:polop"] `shouldMatchList` [All "polop"]
        routeToMessage ["@@: bloop"] `shouldMatchList` [All " bloop"]

    it "route should parse player message" $ do
        routeToMessage ["@0:polop"] `shouldMatchList` [Player 0 "polop"]
        routeToMessage ["@15: bloop"] `shouldMatchList` [Player 15 " bloop"]

    it "route should return Null when errors" $
        mapM_ (\text -> routeToMessage [text] `shouldMatchList` [Null])
            ["@0 :polop"
            , "@ : bloop"
            , ""
            , "greu"
            ]
