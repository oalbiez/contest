module Contest.Match.CommandSpec (spec) where


import           Contest.Match.Command
import           Test.Hspec


spec :: Spec
spec = do

    it "should parse a drop command" $ do
        parse "drop: 3" `shouldBe` DropPlayer 3
        parse "drop:   3   " `shouldBe` DropPlayer 3

    it "should parse a winners command" $
        parse "winners: 3, 5" `shouldBe`  WinningPlayers [3, 5]

    it "should parse a turn command" $
        parse "turn: 4 2 1 50"
            `shouldBe` Ask {turn=4, player=2, expectation=Expectation 1 50}
