module Contest.Match.QueueSpec (spec) where


import           Contest.Match.Queue
import           Control.Lens
import           Test.Hspec


spec :: Spec
spec = do

        it "empty queue should remain unchanged" $
            let queue = empty 4
                      & processQueue
            in queue `shouldBe` empty 4


        it "queue should dispatch messages from referee to players" $
            let queue = empty 4
                      & appendRefereeStdout ["@0:welcome 0"]
                      & appendRefereeStdout ["@2:welcome 2"]
            in do
                getPlayerStdin 0 queue `shouldMatchList` ["welcome 0"]
                getPlayerStdin 1 queue `shouldMatchList` []
                getPlayerStdin 2 queue `shouldMatchList` ["welcome 2"]
                getPlayerStdin 3 queue `shouldMatchList` []


        it "queue should dispatch messages from referee to players on channel @@" $
            let queue = empty 4
                      & appendPlayerStdout 2 ["MOVE"]
            in
                getRefereeStdin queue `shouldMatchList` ["@2:MOVE"]


        it "queue should dispatch messages from player to referee" $
            let queue = empty 4
                      & appendRefereeStdout ["@@:welcome"]
            in do
                getPlayerStdin 0 queue `shouldMatchList` ["welcome"]
                getPlayerStdin 1 queue `shouldMatchList` ["welcome"]
                getPlayerStdin 2 queue `shouldMatchList` ["welcome"]
                getPlayerStdin 3 queue `shouldMatchList` ["welcome"]

        it "queue should dispatch commands from referee" $
            let queue = empty 4
                      & appendRefereeStdout ["/cmd"]
            in getCommands queue `shouldBe` ["cmd"]


        it "queue should dispatch commands in orders" $
            let queue = empty 4
                      & appendRefereeStdout ["/drop: 2","/drop: 3","/drop: 4"]
            in getCommands queue `shouldBe` ["drop: 2", "drop: 3", "drop: 4"]
