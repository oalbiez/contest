module System.Clock.Extra
    ( timeoutOf
    ) where


import qualified System.Clock as Clock


-- | Call once to start a timeout, then call repeatedly to get the remaining time in milliseconds.
--   The time is guaranteed to be monotonic. This function is robust to system time changes.
timeoutOf :: Int -> IO (IO Int)
timeoutOf delay =
    let
        toNano = fromIntegral . Clock.toNanoSecs
        toMilli = (`div` 1000000)
        fromMilli = (* 1000000)
    in do
        limit <- fmap ((+ fromMilli delay) . toNano) time
        pure $ fmap (toMilli . max 0 . (limit -) . toNano) time
        where time = Clock.getTime Clock.Monotonic
