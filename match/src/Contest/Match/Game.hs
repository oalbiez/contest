{-# LANGUAGE QuasiQuotes     #-}
{-# LANGUAGE TemplateHaskell #-}
module Contest.Match.Game
    ( Game(..)
    , create
    , turn
    ) where

import           Contest.Match.Actor
import qualified Contest.Match.Command   as Command
import qualified Contest.Match.Queue     as Q
import           Control.Lens
import           Control.Monad           (foldM, (>=>))
import qualified Data.List               as List
import qualified Data.Map                as Map
import           Data.String.Interpolate (i)


type Players = Map.Map Int Actor


data GameState
    = ProcessingReferee
    | ProcessingPlayer Int Command.Expectation
    | Terminated


data Game
    = Game
    { _referee :: Actor
    , _players :: Players
    , _queue   :: Q.Queue
    , _state   :: GameState
    }
makeLenses ''Game


mkGame :: Actor -> [Actor] -> Game
mkGame referee players = Game
        referee
        (Map.fromList (List.zip [0..] players))
        (Q.empty (length players))
        ProcessingReferee


killPlayer:: Int -> Game -> IO Game
killPlayer id game =
    case game^?players.ix id of
            Nothing      -> return game
            (Just actor) -> do
                terminateActor actor
                return $ game & players.at id .~ Nothing


changeState :: GameState -> Game -> Game
changeState value game = game & state .~ value


create :: String -> [String] -> IO Game
create referee playersImages = do
    referee <- createActor referee
    players <- mapM createActor playersImages
    return $ mkGame referee players


report :: String -> [String] -> IO ()
report prefix = mapM_ (putStrLn . (prefix ++))


processCommands :: Game -> IO Game
processCommands game =
    let command game cmd = case cmd of
            Command.Ask turn player expectation
                -> do
                    putStrLn [i|\n/turn: #{turn}, player: #{player}|]
                    (return . changeState (ProcessingPlayer player expectation)) game
            Command.WinningPlayers ids
                -> do
                    putStrLn [i|\n/winners: #{show ids}|]
                    (return . changeState Terminated) game
            Command.DropPlayer id
                -> do
                    putStrLn [i|\n/drop: #{id}|]
                    killPlayer id game
            _ -> return game
    in
        map Command.parse (Q.getCommands $ game^.queue)
        & foldM command (game & queue %~ Q.resetCommands)


processReferee :: Game -> IO Game
processReferee =
    let
        process game = do
            toInput (game^.referee) (Q.getRefereeStdin $ game^.queue)
            outputs <- fromOutput (game^.referee)
            fromError (game^.referee) >>= report "; "
            return
                $ game
                & queue %~ Q.resetRefereeStdin
                & queue %~ Q.appendRefereeStdout outputs
    in process >=> processCommands


processPlayer :: Int -> Command.Expectation -> Game -> IO Game
processPlayer id expectation game =
    let
        fromOuput = fromOutputWithTimeout
            (Command.linecount expectation)
            (Command.delay expectation)
        addStatus Completed game = game
        addStatus Aborted game
            = game & queue %~ Q.appendRefereeStdin [i|/timeout: #{id}|]
        process actor game = do
            let inputs = Q.getPlayerStdin id $ game^.queue
            toInput actor inputs
            report [i|@#{id}< |] inputs
            (outputs, status) <- fromOuput actor
            fromError actor >>= report [i|@#{id}| |]
            report [i|@#{id}> |] outputs
            return
                $ game
                & queue %~ Q.resetPlayerStdin id
                & queue %~ Q.appendPlayerStdout id outputs
                & changeState ProcessingReferee
                & addStatus status
    in case game^?players.ix id of
        Nothing      -> return game
        (Just actor) -> (process actor >=> processCommands) game


turn :: Game -> IO ()
turn game =
    case game^.state of
        ProcessingReferee               -> processReferee game >>= turn
        ProcessingPlayer id expectation -> processPlayer id expectation game >>= turn
        Terminated                      -> return ()

