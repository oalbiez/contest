module Contest.Match.Command
    ( Command(..)
    , Expectation(..)
    , parse
    ) where


import           Control.Monad    (void)
import qualified Data.Either      as Either
import           Data.Function    ((&))
import qualified Text.Parsec      as Parsec
import qualified Text.Parsec.Char as Parsec.Char


{- | Parser
-}
type Parser = Parsec.Parsec String ()

spaceP :: Parser a -> Parser a
spaceP p = p <* Parsec.Char.spaces

symbolP :: String -> Parser ()
symbolP v = Parsec.try (void $ spaceP $ Parsec.Char.string v)

numberP :: Parser Int
numberP = read <$> spaceP (Parsec.many1 Parsec.Char.digit)

numbersP :: Parser [Int]
numbersP = Parsec.sepBy numberP (symbolP ",")

commandP :: Parser Command
commandP = Parsec.choice
    [ symbolP "drop:" >> DropPlayer <$> numberP
    , symbolP "turn:" >> Ask <$> numberP <*> numberP <*> (Expectation <$> numberP <*> numberP)
    , symbolP "winners:" >>  WinningPlayers <$> numbersP
    ]


data Command
    = Error String
    | Ask {turn:: Int, player:: Int, expectation:: Expectation}
    | DropPlayer Int
    | WinningPlayers [Int]
    deriving (Show, Eq)


data Expectation
    = Expectation
    { linecount :: Int
    , delay     :: Int
    } deriving (Show, Eq)


parse :: String -> Command
parse text = Parsec.parse commandP "" text
           & Either.fromRight (Error text)
