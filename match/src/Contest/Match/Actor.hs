{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TemplateHaskell  #-}
module Contest.Match.Actor
    ( Actor(..)
    , ActorIO(..)
    , OutputOutcome(..)
    ) where


import           Control.Lens
import           Control.Monad      (void)
import qualified Data.Either        as Either
import qualified Data.List.Extra    as ListExtra
import           GHC.IO.Handle      (Handle)
import qualified System.Clock.Extra as ClockExtra
import qualified System.IO          as IO
import           System.Process
import qualified Text.Parsec        as Parsec
import qualified Text.Parsec.Char   as Parsec.Char


data Actor
    = Actor
    { _hin     :: Handle
    , _hout    :: Handle
    , _herr    :: Handle
    , _process :: ProcessHandle
    }
makeLenses ''Actor


command :: String -> CmdSpec
command cmd =
    let
        program prg = RawCommand prg [ ]
        stack prg = RawCommand "stack" [ "run", prg ]
        docker image = RawCommand "/usr/bin/docker"
                [ "run", "--network", "none", "--interactive", image ]

        symbolP v = Parsec.try (void $ Parsec.Char.string v)
        restOfLineP = Parsec.many Parsec.Char.anyChar
        commandP = Parsec.choice
            [ symbolP "docker://" >> docker <$> restOfLineP
            , symbolP "stack://" >> stack <$> restOfLineP
            , symbolP "exec://" >> program <$> restOfLineP
            ]
    in Parsec.parse commandP "" cmd & Either.fromRight (docker cmd)



data OutputOutcome
    = Completed
    | Aborted


class ActorIO m where
    createActor :: String -> m Actor
    terminateActor :: Actor -> m ()
    toInput :: Actor -> [String] -> m ()
    fromOutput :: Actor -> m [String]
    fromOutputWithTimeout :: Int -> Int -> Actor -> m ([String], OutputOutcome)
    fromError :: Actor -> m [String]


instance ActorIO IO where
    createActor cmd = do
        (Just stdin, Just stdout, Just stderr, process) <- createProcess $ CreateProcess
            { std_in = CreatePipe
            , std_out = CreatePipe
            , std_err = CreatePipe
            , cmdspec = command cmd
            , env = Nothing
            , close_fds = False
            , create_group = False
            , delegate_ctlc = False
            , detach_console = False
            , create_new_console = False
            , new_session = False
            , child_group = Nothing
            , child_user = Nothing
            , use_process_jobs = False
            , cwd = Nothing
            }
        mapM_ (`IO.hSetBuffering` IO.LineBuffering) [stdin, stdout]
        return $ Actor stdin stdout stderr process

    terminateActor actor = terminateProcess (actor^.process)

    toInput actor = mapM_ (IO.hPutStrLn (actor^.hin))

    fromOutput actor = do
        line <- IO.hGetLine (actor^.hout)
        remainder <- readAll (actor^.hout)
        return (line : remainder)

    fromError actor = readAll (actor^.herr)

    fromOutputWithTimeout expected delay actor =
        let
            loop handle timeout expected accumulator
                | expected == 0 = return (accumulator, Completed)
                | otherwise = do
                    lineReady <- timeout >>= IO.hWaitForInput handle
                    if lineReady then
                        fmap (ListExtra.snoc accumulator) (IO.hGetLine handle)
                        >>= loop handle timeout (expected - 1)
                    else do
                        IO.hPutStrLn IO.stderr "paf"
                        return (accumulator, Aborted)
        in do
            timeout <- ClockExtra.timeoutOf delay
            loop (actor^.hout) timeout expected []


readAll :: Handle -> IO [String]
readAll handle = do
    ready <- IO.hReady handle
    if not ready
        then return []
        else do
            line <- IO.hGetLine handle
            remainder <- readAll handle
            return (line : remainder)
