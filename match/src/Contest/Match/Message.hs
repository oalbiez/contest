module Contest.Match.Message
    ( Message(..)
    , routeToMessage
    ) where

import           Control.Monad    (void)
import qualified Data.Either      as Either
import           Data.Function    ((&))
import qualified Text.Parsec      as Parsec
import qualified Text.Parsec.Char as Parsec.Char


{- | Parser
-}
type Parser = Parsec.Parsec String ()

symbolP :: String -> Parser ()
symbolP v = Parsec.try (void $ Parsec.Char.string v)

numberP :: Parser Int
numberP = read <$> Parsec.many1 Parsec.Char.digit

restOfLineP :: Parser String
restOfLineP = Parsec.many Parsec.Char.anyChar

messageP :: Parser Message
messageP = Parsec.choice
    [ symbolP "/" >> Command <$> restOfLineP
    , symbolP "@@:" >> All <$> restOfLineP
    , symbolP "@" >> Player <$> (numberP <* symbolP ":") <*> restOfLineP
    ]


data Message
    = Null
    | All String
    | Command String
    | Player Int String
    deriving (Show, Eq)


routeToMessage :: [String] -> [Message]
routeToMessage = map single
    where single line = Parsec.parse messageP "" line
                      & Either.fromRight Null
