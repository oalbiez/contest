{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE QuasiQuotes      #-}
{-# LANGUAGE TemplateHaskell  #-}
module Contest.Match.Queue
    (Queue(..)
    , Buffers(..)
    , appendPlayerStdout
    , appendRefereeStdin
    , appendRefereeStdout
    , empty
    , getCommands
    , getPlayerStdin
    , getRefereeStdin
    , processQueue
    , resetCommands
    , resetPlayerStdin
    , resetRefereeStdin
    ) where

import qualified Contest.Match.Message   as Msg
import           Control.Lens
import qualified Data.List               as List
import qualified Data.Map                as Map
import           Data.String.Interpolate (i)


data Buffers
    = Buffers
    { _stdin  :: [String]
    , _stdout :: [String]
    } deriving (Show, Eq)
makeLenses ''Buffers

emptyBuffers :: Buffers
emptyBuffers = Buffers [] []

data Queue
    = Queue
    { _commands :: [String]
    , _referee  :: Buffers
    , _players  :: Map.Map Int Buffers
    } deriving (Show, Eq)
makeLenses ''Queue

empty :: Int -> Queue
empty nbPlayer
    = Map.fromList [(c, emptyBuffers) | c <- [0..nbPlayer]]
    & Queue [] emptyBuffers

refereeStdout :: Functor f => ([String] -> f [String]) -> Queue -> f Queue
refereeStdout = referee.stdout

refereeStdin :: Applicative f => ([String] -> f [String]) -> Queue -> f Queue
refereeStdin = referee.stdin

player:: Applicative f => Int -> (Buffers -> f Buffers) -> Queue -> f Queue
player p = players.at p.traversed

allPlayers:: Applicative f => (Buffers -> f Buffers) -> Queue -> f Queue
allPlayers = players.traversed

allPlayersStdout :: (Contravariant f, Applicative f) =>
                          ((Int, [String]) -> f (j, [String])) -> Queue -> f Queue
allPlayersStdout = players.ifolded.withIndex.alongside id stdout

addAddress :: [(Int, [String])] -> [String]
addAddress messages =
    let
        from id = map ([i|@#{id}:|] ++)
    in List.concatMap (uncurry from) messages

processMessage :: Queue -> Msg.Message -> Queue
processMessage queue (Msg.All content) = queue & allPlayers.stdin %~ (++ [content])
processMessage queue (Msg.Command content) = queue & commands %~ (++ [content])
processMessage queue (Msg.Player id content) = queue & player id.stdin %~ (++ [content])
processMessage queue _ = queue

processQueue :: Queue -> Queue
processQueue queue =
    let processPlayers queue = queue^.refereeStdout
                             & Msg.routeToMessage
                             & List.foldl processMessage queue
    in queue
        & refereeStdin %~ (++ (queue^..allPlayersStdout & addAddress))
        & allPlayers.stdout.~[]
        & processPlayers
        & refereeStdout.~[]


getCommands :: Queue -> [String]
getCommands queue = queue^.commands


resetCommands :: Queue -> Queue
resetCommands queue = queue & commands .~ []


getRefereeStdin :: Queue -> [String]
getRefereeStdin queue = queue^.refereeStdin


resetRefereeStdin :: Queue -> Queue
resetRefereeStdin queue = queue & refereeStdin .~ []


appendRefereeStdout :: [String] -> Queue -> Queue
appendRefereeStdout lines queue
    = queue
    & refereeStdout %~ (++ lines)
    & processQueue


appendRefereeStdin :: String -> Queue -> Queue
appendRefereeStdin line queue
    = queue
    & refereeStdin %~ (++ [line])
    & processQueue


getPlayerStdin :: Int -> Queue -> [String]
getPlayerStdin id queue = queue^.player id.stdin


resetPlayerStdin :: Int -> Queue -> Queue
resetPlayerStdin id queue = queue & player id.stdin .~ []


appendPlayerStdout :: Int -> [String] -> Queue -> Queue
appendPlayerStdout id lines queue
    = queue
    & player id.stdout %~ (++ lines)
    & processQueue
