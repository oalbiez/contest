module Main where

import qualified Contest.Match.Game  as Game
import           Options.Applicative


data Arguments
    = Arguments
    { referee :: String
    , players :: [String]
    } deriving (Show)


match :: Parser Arguments
match = Arguments
      <$> Options.Applicative.argument str (metavar "REFEREE")
      <*> some (Options.Applicative.argument str (metavar "PLAYERS..."))


opts :: ParserInfo Arguments
opts = info (match <**> helper)
      ( fullDesc
     <> progDesc "Play a match with a referee and some players"
     <> header "match - play a match" )


start :: Arguments -> IO Game.Game
start match =
    Game.create (referee match) (players match)


main :: IO ()
main = execParser opts >>= start >>= Game.turn
