module Referee.Command
    ( Command(..)
    , parse
    ) where

import           Control.Monad    (void)
import qualified Data.Either      as Either
import           Data.Function    ((&))
import qualified Text.Parsec      as Parsec
import qualified Text.Parsec.Char as Parsec.Char


data Command
    = Error String
    | Message Int String
    | Timeout Int
    deriving (Show, Eq)


{- | Parser
-}
type Parser = Parsec.Parsec String ()

spaceP :: Parser a -> Parser a
spaceP p = p <* Parsec.Char.spaces

symbolP :: String -> Parser ()
symbolP v = Parsec.try (void $ spaceP $ Parsec.Char.string v)

numberP :: Parser Int
numberP = read <$> spaceP (Parsec.many1 Parsec.Char.digit)

restOfLineP :: Parser String
restOfLineP = Parsec.many Parsec.Char.anyChar

commandP :: Parser Command
commandP = Parsec.choice
    [ symbolP "/timeout: " >> Timeout <$> numberP
    , symbolP "@" >> Message <$> (numberP <* symbolP ":") <*> restOfLineP
    ]


parse :: String -> Command
parse text = Parsec.parse commandP "" text
           & Either.fromRight (Error text)
