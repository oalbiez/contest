module Referee.CommandSpec (spec) where


import           Referee.Command
import           Test.Hspec


spec :: Spec
spec = do

    it "should parse a message command" $ do
        parse "@1: hello" `shouldBe` Message 1 "hello"
        parse "@13: roger" `shouldBe` Message 13 "roger"

    it "should parse a timeout command" $ do
        parse "/timeout: 4" `shouldBe`  Timeout 4
        parse "/timeout: 42" `shouldBe`  Timeout 42

    it "should parse a turn command" $
        parse "paf" `shouldBe` Error "paf"
